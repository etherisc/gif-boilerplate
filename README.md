# GIF boilerplate (step-by-step guide) 
1. Make sure than you have nodejs (version > 10 && < 13) installed:  
`node --version`  
2. Install gifcli (command line tool to interact with GIF):  
`npm install -g @etherisc/gifcli`
3. Rename .env.sample file to .env and fill required fields:      

Generate mnemomic: https://iancoleman.io/bip39/#english.  
You will need 3 accounts with some ETH.  
Use rinkeby faucet to get some test ETH: https://faucet.rinkeby.io/.  

Register on https://infura.io, create new project and get project id.  

Alternatively you can take one mnemonic from MNEMONICS.md.  

## Installation  
Install root dependencies  
`npm install`

Install apps dependencies  
`npm run bootstrap`

## Registration
Register in GIF:    
`gifcli user:register`  
Register your product:  
`gifcli product:create` 

## Deploy contract
`npm run migrate-rinkeby`  
`npm run networks`  
Register contract in GIF:  
`gifcli artifact:send --file artifacts/BikeStoreInsurance.json --network rinkeby` 
Run console:  
`gifcli console`
Check your product status, it should be approved:  
`> gif.product.get()`    

## Part 1: Interaction with contract from console
Create a new customer:    
`> gif.customer.create({ firstname: "Jow", lastname: "Dow", email: "jow@dow.com" })`  
Start a new business process:  
`> gif.bp.create({ customerId: '<CUSTOMER-ID>', manufacturer: 'Cube', vendorCode: 'K1244', period: 1, price: 1000, premium: 1000, currency: 'EUR'  })`
Get quote:    
`> gif.contract.call("BikeStoreInsurance", "getQuote", [100, 1])`  
Apply for a policy:
`> gif.contract.send("BikeStoreInsurance", "applyForPolicy", [ "Cube", "K1278", 1, 1000, 50, "EUR", "PUT-BP-KEY-HERE"])`  
Check application:  
`> gif.application.getById(<application id>)`
`> gif.application.list()`  
Underwrite the application:  
`> gif.contract.send("BikeStoreInsurance", "underwriteApplication", [<application id>])`  
Check the new policy:  
`> gif.policy.getById(<policy id>)`
`> gif.policy.list()`  
Create a claim for the policy:  
`> gif.contract.send("BikeStoreInsurance", "createClaim", [<policy id>])` 
Check the claim:   
`> gif.claim.getById(<claim id>)`  
`> gif.claim.list()`  
Confirm the claim:  
`> gif.contract.send("BikeStoreInsurance", "confirmClaim", [ <application id>, <claim id>, <payout option> ])`
Check the payout status:  
`> gif.payout.getById(<payout id>)`  
`> gif.payout.list()`  
Confirm payout:  
`> gif.contract.send("BikeStoreInsurance", "confirmPayout", [ <payout id>, <amount> ])` 
Check the final payout status:   
`> gif.payout.getById(<payout id>)`  
`> gif.payout.list()`  

Gifcli also available as importable NPM module to be used from a nodejs app:  
https://gist.github.com/kandrianov/39192414f9ee03af2440c2b5b829639b

## Part 2: Integration with Aragon DAO
For this part you will need Metamask installed (https://metamask.io/) and two accounts in it with small amount of ETH in Rinkeby for transactions.  
Alternatively you can get accounts from MNEMONICS.md and import to Metamask.  

### Deploy new AragonDAO
1. Go to https://rinkeby.aragon.org/, click "Create an organization", use "Membership" template for deployment.  
2. Choose the name for your organization. Next.  
3. Configure voting parameters: set 50% SUPPORT, 50% MINIMUM APPROVAL, set 10 min VOTING DURATION for quick testing. Next.  
4. Set TOKEN NAME, add 3 tokenholders (from MNEMONIC)
5. Confirm proposed transactions to deploy contracts and launch the organization.  
6. Navigate to "Organization" and copy the address of the voting contract and set it as ARAGON_VOTING_CONTRACT variable in .env file  

In this part we will deploy another contract. In the file "./smart-contracts/migrations/2_deploy_BikeStoreInsurance" comment lines for
the first part and uncomment lines for the second part.

### Deploy contracts  
Create new product:
`gifcli product:create`  
Deploy contracts:  
`npm run migrate-rinkeby` 
Register contract in GIF:  
`gifcli artifact:send --file artifacts/BikeStoreInsuranceWithVoting.json --network rinkeby`

Check the address of deployed contract.  
Go to AragonDAO, navigate to "Permissions", click "New permission". Assing "Create new votes" permission to address of 
BikeStoreInsuranceWithVoting contract. Confirm voting. Navigate to “Voting”. Vote “Yes” from two accounts that you have granted tokens.  

### Start applications  
`npm run start`  
Open "http://0.0.0.0:8081/"  

 - Apply for a policy, go to dashboard and check application status
 - Underwrite policy from dashboard
 - Vote from AragonDAO for underwriting policy from two accounts  
 - Check policy status
 - Create a claim
 - Confirm the claim from dashboard
 - Create payout from dashboard
