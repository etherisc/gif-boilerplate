const HDWalletProvider = require("truffle-hdwallet-provider")
const Web3 = require('web3')

module.exports = async () => {
  const numAccounts = process.env.NUM_ACCOUNTS || 1
  const providerUrl = `https://${process.env.ETHEREUM_NETWORK}.infura.io/v3/${process.env.INFURA_PROJECT_ID}`

  const provider = new HDWalletProvider(process.env.MNEMONIC, providerUrl, 0, numAccounts, true)
  const web3 = new Web3(provider)
  web3.networkId = await web3.eth.net.getId()
  web3.accounts = await web3.eth.getAccounts()

  return web3
}
