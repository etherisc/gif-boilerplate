const log = require('./log');
const web3client = require('./web3')
const Contracts = require('./contracts')

async function Initialize(dotConfig) {
  const web3 = await web3client()
  const contracts = new Contracts(web3, log)
  await contracts.initialize()

  const config = { ...dotConfig };

  log.info('Network:', web3.networkId)
  log.info('Accounts:')
  log.info(web3.accounts)
  log.info('Contracts:')
  for (let contract of Object.keys(contracts.contracts)) {
    log.info('  ', contract, contracts.contracts[contract].address)
  }


  return {
    log,
    config,
    web3,
    contracts,
  };
}

module.exports = { Initialize };
