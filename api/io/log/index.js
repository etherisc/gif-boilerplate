const log = {
  info: console.log,
  error: console.error,
}

module.exports = log
