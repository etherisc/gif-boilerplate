const fs = require('fs')
const path = require('path')

class Contracts {
  constructor(web3, log) {
    this.log = log
    this.web3 = web3
    this.contracts = {}
    this.contractByAddress = {}

    this.currentAccountInc = 0
    this.gasPrice = 10 * (10 ** 9)
  }

  nextAccount() {
    const accounts = this.web3.accounts
    const from = accounts[this.currentAccountInc % accounts.length]

    this.currentAccountInc++

    return from
  }

  async initialize() {
    const files = fs.readdirSync(path.resolve('../artifacts'))
      .filter(filename => filename.endsWith('.json'))

    for (let i = 0; i < files.length; i++) {
      const file = files[i]

      const content = fs.readFileSync(path.resolve('../artifacts', file))
      const { networks, abi, contractName } = JSON.parse(content)

      if (networks[this.web3.networkId]) {
        const { address, transactionHash } = networks[this.web3.networkId]

        const contractAddr = this.web3.utils.toChecksumAddress(address)

        const { blockNumber } = await this.web3.eth.getTransactionReceipt(transactionHash)

        this.contracts[contractName] = {
          address: contractAddr,
          transactionHash, blockNumber, abi,
          instance: new this.web3.eth.Contract(abi, contractAddr),
        }

        this.contractByAddress[contractAddr] = contractName
      }
    }

    if (process.env.ARAGON_VOTING_CONTRACT) {
      const address = this.web3.utils.toChecksumAddress(process.env.ARAGON_VOTING_CONTRACT)

      const votingArtefact = fs.readFileSync(path.resolve('../artifacts', 'IVoting.json'))
      const { abi } = JSON.parse(votingArtefact)

      this.contracts.IVoting = {
        address,
        transactionHash: null,
        blockNumber: null,
        abi,
        instance: new this.web3.eth.Contract(abi, address),
      }

      this.contractByAddress[address] = 'IVoting'
    }
  }

  getContracts() {
    return Object.keys(this.contractByAddress)
  }

  decodeEvent(event) {
    const contract = this.contracts[this.contractByAddress[event.address]]

    const abi = contract.abi
      .filter(i => i.type === 'event')
      .map(i => Object.assign(i, { signature: this.web3.eth.abi.encodeEventSignature(i) }))
      .filter(i => i.signature === event.topics[0])

    const data = this.web3.eth.abi.decodeLog(abi[0].inputs, event.data, event.topics.slice(1));

    return { name: abi[0].name, abi: abi[0], data, raw: event }
  }

  call(contractName, method, params) {
    this.log.info('CALL:', contractName, method, params)

    const dest = this.contracts[contractName].instance.methods[method](...params)

    return dest.call()
  }

  async send(contractName, method, params) {
    const from = this.nextAccount()
    this.log.info('SEND TX:', contractName, method, params, 'from', from)

    try {
      const nonce = await this.web3.eth.getTransactionCount(from)
      const dest = this.contracts[contractName].instance.methods[method](...params)
      const gas = await dest.estimateGas({ from })

      await new Promise((resolve, reject) => {
        dest.send({
          nonce,
          from,
          gasPrice: this.gasPrice,
          gas,
        })
          .on('confirmation', (confirmation, receipt) => resolve(receipt))
          .on('receipt', receipt => resolve(receipt))
          .on('error', error => reject(error))
          .catch((error) => {
            if (error.code === 'INVALID_ARGUMENT') {
              const types = error.value.types.map(type => `${type.type} ${type.name}`);
              reject(new Error(`Expected ${error.count.types} arguments (${types.join(', ')}), but ${error.count.values} values provided (${error.value.values.join(', ')})`));
            }

            reject(error);
          });
      });
    } catch (e) {
      throw new Error(e)
    }
  }

}

module.exports = Contracts
