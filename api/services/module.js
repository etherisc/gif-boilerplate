const EthListener = require('./ethListener')

async function Initialize(io) {
  const ethListener = new EthListener(io.web3, io.contracts, io.log)

  return {
    ethListener,
  };
}

module.exports = { Initialize };
