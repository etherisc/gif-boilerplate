class EthListener {
  constructor(web3, contracts, log) {
    this.web3 = web3
    this.contracts = contracts
    this.log = log
    this.pullInterval = 10 * 1000 // 10 seconds

    this.subscribtions = {}
  }

  Subscribe(eventName, handler) {
    this.subscribtions[eventName] = handler
  }

  async handleEvent(event) {
    const decodedEvent = this.contracts.decodeEvent(event)

    if (this.subscribtions[decodedEvent.name]) {
      const fn = this.subscribtions[decodedEvent.name]

      await fn(decodedEvent)
    }
  }

  async processEvents({ fromBlock, toBlock }) {
    try {
      const contractAddresses = this.contracts.getContracts()

      if (contractAddresses.length > 0) {
        const events = await this.web3.eth.getPastLogs({
          fromBlock: this.web3.utils.toHex(fromBlock),
          toBlock: this.web3.utils.toHex(toBlock),
          address: contractAddresses,
        });

        for (let index = 0; index < events.length; index += 1) {
          await this.handleEvent(events[index]);
        }
      }
    } catch (e) {
      this.log.error(new Error(JSON.stringify({ message: e.message, stack: e.stack })));
    }
  }

  async Start() {
    let fromBlock = await this.web3.eth.getBlockNumber() + 1

    setInterval(async () => {
      const toBlock = await this.web3.eth.getBlockNumber()
      this.log.info(`Current block number: ${toBlock}`)
      if (toBlock >= fromBlock) {
        this.log.info(`Pulling events for blocks ${fromBlock} - ${toBlock}`)
        await this.processEvents({ fromBlock, toBlock })
        fromBlock = toBlock + 1;
      }
    }, this.pullInterval)

    this.log.info('EthListener started')
  }
}

module.exports = EthListener
