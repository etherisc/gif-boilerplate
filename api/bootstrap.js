require('dotenv').config({ path: '../.env' })

const { bootstrap } = require('@etherisc/microservice');
const BikeStoreInsurance = require('./BikeStoreInsurance');

bootstrap(BikeStoreInsurance, {
    httpDevPort: 3010,
    log: true,
    websocket: true,
});

