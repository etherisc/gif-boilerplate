const gif = require('@etherisc/gifcli');
const ioDeps = require('./io/module')
const servicesModule = require('./services/module')

class BikeStoreInsurance {
    constructor({ websocket, log }) {
        this.websocket = websocket;
        this.log = log;
    }

    /**
     * On application started livecycle hook
     * @return {Promise<void>}
     */
    async bootstrap() {
        this.gif = await gif.connect();
        this.websocket.setHandler(this.onWsMessage.bind(this));

        this.io = await ioDeps.Initialize()
        const services = await servicesModule.Initialize(this.io)

        const { ethListener } = services

        ethListener.Subscribe('StartVote', async (evt) => {
            const voteId = evt.data.voteId
            const applicationId = await this.gif.contract.call('BikeStoreInsuranceWithVoting', 'voteIdToApplicationId', [voteId])
            const voteTime = await this.io.contracts.call('IVoting', 'voteTime', [])

            setTimeout(async () => {
                await this.gif.contract.send('BikeStoreInsuranceWithVoting', 'resolveApplication', [applicationId]).catch(console.log)
            }, (Number(voteTime) + 5) * 1000)
        })

        ethListener.Subscribe('ExecuteVote', async (evt) => {
            const voteId = evt.data.voteId

            await this.gif.contract.send('BikeStoreInsuranceWithVoting', 'resolveApplication', [voteId])
        })

        ethListener.Start()
    }

    /**
     * Handle message form websocket
     * @param {string} client
     * @param {string} payload
     */
    onWsMessage(client, payload) {
        const message = JSON.parse(payload);
        const { id, type, data } = message;
        const handler = this[type].bind(this);

        if (!id) {
            this.log.error('Id should be provided', type, message);
        }

        if (!type) {
            this.log.error('Invalid message type', type, message);
            return;
        }

        this.log.info('Ws request', id, type, data)
        handler(client, { id, data });
    }

    async newPolicy(client, message) {
        const { customer, policy } = message.data

        const newCustomer = await this.gif.customer.create({
            firstname: customer.firstname,
            lastname: customer.lastname,
            email: customer.email,
        })

        this.websocket.send(client, { data: { operation: 'CREATE CUSTOMER', customerId: newCustomer.customerId } })

        const newBusinessProcess = await this.gif.bp.create({
            customerId: newCustomer.customerId,
            manufacturer: policy.manufacturer,
            vendorCode: policy.vendorCode,
            period: policy.period,
            price: policy.price,
            premium: policy.premium,
            currency: policy.currency,
        })

        this.websocket.send(client, { data: { operation: 'CREATE BUSINESS PROCESS', bp: newBusinessProcess.bpExternalKey } })

        const tx = await this.gif.contract.send(
            "BikeStoreInsuranceWithVoting",
            "applyForPolicy",
            [
                policy.manufacturer,
                policy.vendorCode,
                policy.period,
                policy.price,
                policy.premium,
                policy.currency,
                newBusinessProcess.bpExternalKey
            ]
        )

        this.websocket.send(client, {
            id: message.id,
            data: {
                applicationId: parseInt(tx.events.LogNewApplication.returnValues.applicationId._hex, 16),
                transactionHash: tx.transactionHash,
            }
        });
    }

    async underwrite(client, message) {
        const voteId = await this.gif.contract.call('BikeStoreInsuranceWithVoting', 'applicationIdToVoteId', [message.data.id])

        await this.io.contracts.send('IVoting', 'vote', [voteId[''], true, true])

        this.websocket.send(client, {
            id: message.id,
            data: {}
        });
    }

    decline(client, message) {}

    async newClaim(client, message) {
        const tx = await this.gif.contract.send(
            "BikeStoreInsuranceWithVoting",
            "createClaim",
            [
                message.data.policyId,
            ]
        )

        this.websocket.send(client, {
            id: message.id,
            data: {
                claimId: parseInt(tx.events.LogNewClaim.returnValues.claimId._hex, 16),
                policyId: parseInt(tx.events.LogNewClaim.returnValues.policyId._hex, 16),
            }
        });
    }

    expire(client, message) {}

    rejectClaim(client, message) {}

    async confirmPayout(client, message) {
        await this.gif.contract.send(
            "BikeStoreInsuranceWithVoting",
            "confirmPayout",
            [
                message.data.id,
                message.data.details,
            ]
        )

        this.websocket.send(client, {
            id: message.id,
            data: {}
        });
    }

    async confirmClaim(client, message) {
        const claim = await this.gif.claim.getById(message.data.id)

        const tx = await this.gif.contract.send(
            "BikeStoreInsuranceWithVoting",
            "confirmClaim",
            [
                claim.metadataId,
                message.data.id,
                message.data.details,
            ]
        )

        this.websocket.send(client, {
            id: message.id,
            data: {}
        });
    }

    async getApplications(client, message) {
        const applications = []

        const list = await this.gif.application.list()

        for (const application of list) {
            const bp = await this.gif.bp.getById(application.metadataId)

            applications.push({
                applicationId: application.id,
                state: application.state,
                stateMessage: application.stateMessage,
                payoutOptions: application.payoutOptions,
                createdAt: application.createdAt,
                updatedAt: application.updatedAt,
                manufacturer: bp.manufacturer,
                vendorCode: bp.vendorCode,
                period: bp.period,
                premium: bp.premium,
                price: bp.price,
                currency: bp.currency,
            })
        }

        this.websocket.send(client, { id: message.id, applications });
    }

    async getPolicies(client, message) {
        const policies = [];

        const list = await this.gif.policy.list()

        for (const policy of list) {
            const bp = await this.gif.bp.getById(policy.metadataId)

            policies.push({
                policyId: policy.id,
                state: policy.state,
                stateMessage: policy.stateMessage,
                createdAt: policy.createdAt,
                updatedAt: policy.updatedAt,
                manufacturer: bp.manufacturer,
                vendorCode: bp.vendorCode,
                period: bp.period,
                premium: bp.premium,
                price: bp.price,
                currency: bp.currency,
                applicationId: bp.applicationId,
            })
        }

        this.websocket.send(client, { id: message.id, policies });
    }

    async getClaims(client, message) {
        const claims = []

        const list = await this.gif.claim.list()

        for (const claim of list) {
            const application = await this.gif.application.getById(claim.metadataId)
            const bp = await this.gif.bp.getById(claim.metadataId)

            claims.push({
                applicationId: claim.metadataId,
                claimId: claim.id,
                state: claim.state,
                stateMessage: claim.stateMessage,
                createdAt: claim.createdAt,
                updatedAt: claim.updatedAt,
                payoutOptions: JSON.parse(application.payoutOptions),
                policyId: bp.policyId,
                currency: bp.currency,
            })
        }

        this.websocket.send(client, { id: message.id, claims });
    }

    async getPayouts(client, message) {
        const payouts = await this.gif.payout.list()
        this.websocket.send(client, { id: message.id, payouts });
    }
}

module.exports = BikeStoreInsurance
