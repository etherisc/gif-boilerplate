export const APPLICATION_STATE = {
  0: {
    label: 'Applied',
    actions: [
      {
        id: 1, method: 'underwrite', label: 'Underwrite', intent: 'success',
      },
    ],
  },
  1: {
    label: 'Revoked',
    actions: [],
  },
  2: {
    label: 'Underwritten',
    actions: [],
  },
  3: {
    label: 'Declined',
    actions: [],
  },
}

export const POLICY_STATE = {
  0: {
    label: 'Active',
    actions: [],
  },
  1: {
    label: 'Expired',
    actions: [],
  }
}

export const CLAIM_STATE = {
  0: {
    label: 'Applied',
    actions: [
      // {
      //   id: 5, method: 'rejectClaim', label: 'Reject', intent: 'danger', withDetails: true,
      // },
      {
        id: 6, method: 'confirmClaim', label: 'Confirm', intent: 'warning', withOptions: true,
      },
    ],
  },
  1: {
    label: 'Confirmed',
    actions: [],
  },
  2: {
    label: 'Declined',
    actions: [],
  },
};

export const PAYOUT_STATE = {
  0: {
    label: 'Expected',
    actions: [
      {
        id: 7, method: 'confirmPayout', label: 'Confirm payout', intent: 'success', withAmount: true,
      }
    ],
  },
  1: {
    label: 'PaidOut',
    actions: [],
  },
}
