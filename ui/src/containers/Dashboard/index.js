import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Spinner } from 'evergreen-ui';
import ConfirmationDialog from 'components/ConfirmationDialog';
import moment from 'moment';
import numeral from 'numeral';
import { APPLICATION_STATE, POLICY_STATE, CLAIM_STATE, PAYOUT_STATE } from './config';


/**
 * Decode hex to string
 * @param {string} value
 * @return {string}
 */
function hexToUtf8(value) {
  const hex = value.toString();

  let str = '';
  for (let i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2) {
    str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
  }

  return str;
}

const Article = styled.div`
  height: 100%;
  display: flex;
  padding: 60px 0 0 0;
  position: relative;
`;


const Column = styled.div`
  width: 50%;
  background: #ffebcd;
  border-left: 1px solid #dcc19e;
  height: 100%;
  overflow-y: scroll;
  position: relative;
`;

const Title = styled.div`
  background: #f2d4af;
  font-weight: bold;
  padding: 20px;
  position: fixed;
  width: 100%;
  z-index: 1;
`;

const List = styled.div`
  margin-top: 80px;
`;

const Item = styled.div`
  background: #fff;
  padding: 15px;
  margin: 20px;
  box-shadow: 0 2px 6px 0 rgba(0,0,0,0.12);
  border-radius: 3px;
  
  div {
    padding: 3px 0;
    font-size: 15px;
  }
`;

const Actions = styled.div`
  margin-top: 20px;
  display: flex;
`;

const SpinnerWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 30px;
`;

/**
 * Dashboard page
 */
class Dashboard extends Component {
  state = {
    applications: [],
    policies: [],
    claims: [],
    payouts: [],
    loadingPolicies: true,
    loadingClaims: true,
    loadingApplications: true,
    loadingPayouts: true,
  };

  static propTypes = {
    request: PropTypes.func.isRequired,
  };

  /**
   * On component mounted livecycle hook
   */
  componentDidMount() {
    this.loadApplications();
    this.loadPolicies();
    this.loadClaims();
    this.loadPayouts();

    const interval = 15 * 1000
    this.i1 = setInterval(() => this.loadApplications(), interval)
    this.i2 = setInterval(() => this.loadPolicies(), interval)
    this.i3 = setInterval(() => this.loadClaims(), interval)
    this.i4 = setInterval(() => this.loadPayouts(), interval)
  }

  componentWillUnmount() {
    clearInterval(this.i1)
    clearInterval(this.i2)
    clearInterval(this.i3)
    clearInterval(this.i4)
  }

  /**
   * Update policies and claims lists after transaction
   * @param {{}} data
   */
  updateAfterTransaction = (data) => {
    const { claims, policies } = this.state;

    if (data.policy) {
      const index = policies.findIndex(policy => Number(policy.policyId) === Number(data.policy.policyId));

      const list = [
        ...policies.slice(0, index),
        data.policy,
        ...policies.slice(index + 1),
      ];

      this.setState({ policies: [...list] });
    }

    if (data.claim) {
      const index = claims.findIndex(claim => Number(claim.claimId) === Number(data.claim.claimId));

      const list = [
        ...claims.slice(0, index),
        data.claim,
        ...claims.slice(index + 1),
      ];

      this.setState({ claims: [...list] });
    }
  };

  /**
   * Load the list of all applications
   */
  loadApplications = () => {
    const { request } = this.props;

    if (window.socket && window.socket.isOpened) {
      request('getApplications')
          .then(data => this.setState({ applications: data.applications, loadingApplications: false }))
          .catch(console.error);
    } else {
      setTimeout(() => this.loadApplications(), 100);
    }
  }

  /**
   * Load the list of all policies
   */
  loadPolicies = () => {
    const { request } = this.props;

    if (window.socket && window.socket.isOpened) {
      request('getPolicies')
        .then(data => this.setState({ policies: data.policies, loadingPolicies: false }))
        .catch(console.error);
    } else {
      setTimeout(() => this.loadPolicies(), 100);
    }
  }

  /**
   * Load the list of all claims
   */
  loadClaims = () => {
    const { request } = this.props;
    if (window.socket && window.socket.isOpened) {
      request('getClaims')
        .then(data => this.setState({ claims: data.claims, loadingClaims: false }))
        .catch(console.error);
    } else {
      setTimeout(() => this.loadClaims(), 100);
    }
  }

  /**
   * Load the list of all payouts
   */
  loadPayouts = () => {
    const { request } = this.props;
    if (window.socket && window.socket.isOpened) {
      request('getPayouts')
          .then(data => this.setState({ payouts: data.payouts, loadingPayouts: false }))
          .catch(console.error);
    } else {
      setTimeout(() => this.loadPayouts(), 100);
    }
  }

  /**
   * Render component
   * @return {*}
   */
  render() {
    const {
      applications, policies, claims, payouts, loadingPolicies, loadingClaims, loadingApplications, loadingPayouts,
    } = this.state;
    const { request } = this.props;

    const applicationsList = applications.sort((a, b) => {
      if (a.applicationId > b.applicationId) return -1
      if (a.applicationId < b.applicationId) return 1
    }).map(application => (
        <Item key={application.applicationId}>
          <div><b>{application.manufacturer}</b></div>
          <div>Application ID: {application.applicationId}</div>
          <div>Vendor code: <b>{application.vendorCode}</b></div>
          <div>State: {APPLICATION_STATE[application.state].label}</div>
          {application.stateMessage && <div>State message: {application.stateMessage}</div>}
          <div>Period: {application.period} year(s)</div>
          <div>Price: {numeral(application.price).format('0,0.00')} {application.currency}</div>
          <div>Premium: {numeral(application.premium).format('0,0.00')} {application.currency}</div>
          <div>Created: {moment.unix(application.createdAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>
          <div>Updated: {moment.unix(application.updatedAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>

          {APPLICATION_STATE[application.state].actions.length > 0 && (
            <Actions>
                {APPLICATION_STATE[application.state].actions.map(action => (
                    <ConfirmationDialog
                        key={action.id}
                        action={action.method}
                        label={action.label}
                        intent={action.intent || 'success'}
                        request={request}
                        withDetails={action.withDetails || false}
                        id={String(application.applicationId)}
                        updateAfterTransaction={this.updateAfterTransaction}
                    />
                ))}
            </Actions>
          )}
        </Item>
    ))

    const policiesList = policies.sort((a, b) => {
      if (a.policyId > b.policyId) return -1
      if (a.policyId < b.policyId) return 1
    }).map(policy => (
      <Item key={policy.policyId}>
        <div><b>{policy.manufacturer}</b></div>
        <div>Policy ID: {policy.policyId}</div>
        <div>Application ID: {policy.applicationId}</div>
        <div>Vendor code: <b>{policy.vendorCode}</b></div>
        <div>State: {POLICY_STATE[policy.state].label}</div>
        {policy.stateMessage && <div>State message: {policy.stateMessage}</div>}
        <div>Period: {policy.period} year(s)</div>
        <div>Price: {numeral(policy.price).format('0,0.00')} {policy.currency}</div>
        <div>Premium: {numeral(policy.premium).format('0,0.00')} {policy.currency}</div>
        <div>Created: {moment.unix(policy.createdAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>
        <div>Updated: {moment.unix(policy.updatedAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>

        {POLICY_STATE[policy.state].actions.length > 0 && (
          <Actions>
            {POLICY_STATE[policy.state].actions.map(action => (
              <ConfirmationDialog
                key={action.id}
                action={action.method}
                label={action.label}
                intent={action.intent || 'success'}
                request={request}
                withDetails={action.withDetails || false}
                id={String(policy.policyId)}
                updateAfterTransaction={this.updateAfterTransaction}
              />
            ))}
          </Actions>
        )}
      </Item>
    ));

    const claimsList = claims.sort((a, b) => {
      if (a.claimId > b.claimId) return -1
      if (a.claimId < b.claimId) return 1
    }).map(claim => (
      <Item key={claim.claimId}>
        <div>
          <b>Claim ID: {claim.claimId}</b>
        </div>
        <div>Policy ID: {claim.policyId}</div>
        <div>Payout options: {claim.payoutOptions.join(' / ')}</div>
        <div>Currency {claim.currency}</div>
        <div>State: {CLAIM_STATE[claim.state].label}</div>
        {claim.stateMessage && <div>State message: {hexToUtf8(claim.stateMessage)}</div>}
        <div>Created: {moment.unix(claim.createdAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>
        <div>Updated: {moment.unix(claim.updatedAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>

        {CLAIM_STATE[claim.state].actions.length > 0 && (
          <Actions>
            {CLAIM_STATE[claim.state].actions.map(action => (
              <ConfirmationDialog
                key={action.id}
                action={action.method}
                label={action.label}
                intent={action.intent || 'success'}
                request={request}
                withDetails={action.withDetails || false}
                withOptions={action.withOptions || false}
                withAmount={action.withAmount || false}
                id={String(claim.claimId)}
                options={claim.payoutOptions}
                updateAfterTransaction={this.updateAfterTransaction}
              />
            ))}
          </Actions>
        )}
      </Item>
    ));

    const payoutsList = payouts.sort((a, b) => {
      if (a.id > b.id) return -1
      if (a.id < b.id) return 1
    }).map(payout => (
        <Item key={payout.id}>
          <div>
            <b>Payout ID: {payout.id}</b>
          </div>
          <div>Claim ID: {payout.claimId}</div>
          <div>State: {PAYOUT_STATE[payout.state].label}</div>
          {payout.stateMessage && <div>State message: {hexToUtf8(payout.stateMessage)}</div>}
          <div>Expected amount: {payout.expectedAmount}</div>
          <div>Actual amount: {payout.actualAmount}</div>
          <div>Created: {moment.unix(payout.createdAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>
          <div>Updated: {moment.unix(payout.updatedAt).utc().format('YYYY-MM-DD HH:mm:ss')} UTC</div>

          {PAYOUT_STATE[payout.state].actions.length > 0 && (
              <Actions>
                {PAYOUT_STATE[payout.state].actions.map(action => (
                    <ConfirmationDialog
                        key={action.id}
                        action={action.method}
                        label={action.label}
                        intent={action.intent || 'success'}
                        request={request}
                        withDetails={action.withDetails || false}
                        withOptions={action.withOptions || false}
                        withAmount={action.withAmount || false}
                        id={String(payout.id)}
                        updateAfterTransaction={this.updateAfterTransaction}
                    />
                ))}
              </Actions>
          )}
        </Item>
    ));

    return (
      <Article>
        <Helmet>
          <title>Dashboard</title>
        </Helmet>

        <Column>
          <Title>Applications</Title>

          <List>
            {loadingApplications && <SpinnerWrapper><Spinner /></SpinnerWrapper>}
            {!loadingApplications && applicationsList}
          </List>
        </Column>


        <Column>
          <Title>Policies</Title>

          <List>
            {loadingPolicies && <SpinnerWrapper><Spinner /></SpinnerWrapper>}
            {!loadingPolicies && policiesList}
          </List>
        </Column>

        <Column>
          <Title>Claims</Title>
          <List>
            {loadingClaims && <SpinnerWrapper><Spinner /></SpinnerWrapper>}
            {!loadingPolicies && claimsList}
          </List>
        </Column>

        <Column>
          <Title>Payouts</Title>

          <List>
            {loadingPayouts && <SpinnerWrapper><Spinner /></SpinnerWrapper>}
            {!loadingPayouts && payoutsList}
          </List>
        </Column>
      </Article>
    );
  }
}

export default Dashboard;
