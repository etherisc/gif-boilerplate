import { createGlobalStyle } from 'styled-components';


const GlobalStyles = createGlobalStyle`
  body {
    background: #f7f8fa;
    font-family: 'Open Sans', sans-serif;
  }
  
  html, body, #app {
    height: 100%;
  }
  
  .📦fnt-sze_12px {
    font-size: 14px !important;
  }
`;

export default GlobalStyles;
