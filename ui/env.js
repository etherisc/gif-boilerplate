const env = {
  PORT: 8081,
  PROXY_API_PORT: 3010,
  NETWORK: 'rinkeby',
};

module.exports = env;
