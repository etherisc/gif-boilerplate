pragma solidity ^0.5.8;

interface IVoting {
    event StartVote(uint256 indexed voteId, address indexed creator, string metadata);
    event CastVote(uint256 indexed voteId, address indexed voter, bool _supports, uint256 stake);
    event ExecuteVote(uint256 indexed voteId);
    event ChangeSupportRequired(uint64 supportRequiredPct);
    event ChangeMinQuorum(uint64 minAcceptQuorumPct);
    event ScriptResult(address indexed executor, bytes script, bytes input, bytes returnData);

    function newVote(bytes calldata _executionScript, string calldata _metadata)
    external
    returns (uint256 voteId);

    function newVote(
        bytes calldata _executionScript,
        string calldata _metadata,
        bool _castVote,
        bool _executesIfDecided
    ) external returns (uint256 voteId);

    function vote(uint256 _voteId, bool _supports, bool _executesIfDecided)
    external;

    function executeVote(uint256 _voteId) external;

    function canExecute(uint256 _voteId) external view returns (bool);

    function getVote(uint256 _voteId)
    external
    view
    returns (
        bool open,
        bool executed,
        uint64 startDate,
        uint64 snapshotBlock,
        uint64 supportRequired,
        uint64 minAcceptQuorum,
        uint256 yea,
        uint256 nay,
        uint256 votingPower,
        bytes memory script
    );

    function voteTime() external view returns (uint256);
}
