pragma solidity ^0.5.8;

import "@etherisc/gif/contracts/Product.sol";

contract BikeStoreInsurance is Product {
    /* Event */
    event LogNewApplication(uint256 applicationId);
    event LogApplicationDeclined(uint256 applicationId);
    event LogNewPolicy(uint256 policyId);
    event LogNewClaim(uint256 policyId, uint256 claimId);
    event LogClaimDeclined(uint256 claimId);
    event LogNewPayoutRequired(uint256 payoutId);
    event LogPayout(uint256 payoutId, uint256 amount);

    bytes32 public constant NAME = "BikeStoreInsurance";
    bytes32 public constant POLICY_FLOW = "PolicyFlowDefault";

    /* Risk definition */
    struct Risk {
        bytes32 manufacturer;
        bytes32 productCode;
        uint256 period;
    }

    mapping(bytes32 => Risk) public risks;

    /* Constructor */
    constructor(address _productService) public Product(_productService, NAME, POLICY_FLOW) {}

    function getQuote(uint256 _price, uint256 _period) public pure returns (uint256 _premium) {
        require(_price > 0, "ERROR::INVALID PRICE");
        require(_period >= 1 && _period <= 3, "ERROR::INVALID PERIOD");
        _premium = _price.mul(_period).div(20);
    }

    function applyForPolicy(
        bytes32 _manufacturer,
        bytes32 _productCode,
        uint256 _period,
        uint256 _price,
        uint256 _premium,
        bytes32 _currency,
        bytes32 _bpExternalKey
    ) external onlySandbox {
        require(_premium > 0, "ERROR:INVALID_PREMIUM");
        require(getQuote(_price, _period) == _premium, "ERROR::INVALID_PREMIUM");

        bytes32 riskId = keccak256(abi.encodePacked(_manufacturer, _productCode, _period));
        risks[riskId] = Risk(_manufacturer, _productCode, _period);

        uint256[] memory payoutOptions = new uint256[](3);
        payoutOptions[0] = _price.div(4);
        payoutOptions[1] = _price.div(2);
        payoutOptions[2] = _price;

        uint256 applicationId = _newApplication(
            _bpExternalKey,
            _premium,
            _currency,
            payoutOptions
        );

        emit LogNewApplication(applicationId);
    }

    function underwriteApplication(uint256 _applicationId) external onlySandbox {
        uint256 policyId = _underwrite(_applicationId);
        emit LogNewPolicy(policyId);
    }

    function declineApplication(uint256 _applicationId) external onlySandbox {
        _decline(_applicationId);
        emit LogApplicationDeclined(_applicationId);
    }

    function createClaim(uint256 _policyId) external onlySandbox {
        uint256 claimId = _newClaim(_policyId);
        emit LogNewClaim(_policyId, claimId);
    }

    function confirmClaim(uint256 _applicationId, uint256 _claimId, uint256 _payoutOption) external onlySandbox {
        uint256[] memory payoutOptions = _getPayoutOptions(_applicationId);
        uint256 payoutId = _confirmClaim(_claimId, payoutOptions[_payoutOption]);

        emit LogNewPayoutRequired(payoutId);
    }

    function confirmPayout(uint256 _payoutId, uint256 _amount) external onlySandbox {
        _payout(_payoutId, _amount);

        emit LogPayout(_payoutId, _amount);
    }
}
