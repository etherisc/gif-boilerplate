pragma solidity ^0.5.8;

import "@etherisc/gif/contracts/Product.sol";
import "./IVoting.sol";

contract BikeStoreInsuranceWithVoting is Product {
    /* Event */
    event LogNewApplication(uint256 applicationId);
    event LogApplicationDeclined(uint256 applicationId);
    event LogNewPolicy(uint256 policyId);
    event LogNewClaim(uint256 policyId, uint256 claimId);
    event LogClaimDeclined(uint256 claimId);
    event LogNewPayoutRequired(uint256 payoutId);
    event LogPayout(uint256 payoutId, uint256 amount);
    event LogNewVotingForApplication(uint256 applicationId, uint256 voteId);

    /* Aragon DAO Voting app */
    IVoting public voting;

    bytes32 public constant NAME = "BikeStoreInsurance";
    bytes32 public constant POLICY_FLOW = "PolicyFlowDefault";

    /* Risk definition */
    struct Risk {
        bytes32 manufacturer;
        bytes32 productCode;
        uint256 period;
    }

    mapping(bytes32 => Risk) public risks;

    // AragonDAO voteId => GIF applicationId
    mapping(uint256 => uint256) public voteIdToApplicationId;
    mapping(uint256 => uint256) public applicationIdToVoteId;

    /* Constructor */
    constructor(address _productService, IVoting _voting) public Product(_productService, NAME, POLICY_FLOW) {
        voting = _voting;
    }

    function getQuote(uint256 _price, uint256 _period) public pure returns (uint256 _premium) {
        require(_price > 0, "ERROR::INVALID PRICE");
        require(_period >= 1 && _period <= 3, "ERROR::INVALID PERIOD");
        _premium = _price.mul(_period).div(20);
    }

    function applyForPolicy(
        bytes32 _manufacturer,
        bytes32 _productCode,
        uint256 _period,
        uint256 _price,
        uint256 _premium,
        bytes32 _currency,
        bytes32 _bpExternalKey
    ) external onlySandbox {
        require(_premium > 0, "ERROR:INVALID_PREMIUM");
        require(getQuote(_price, _period) == _premium, "ERROR::INVALID_PREMIUM");

        bytes32 riskId = keccak256(abi.encodePacked(_manufacturer, _productCode, _period));
        risks[riskId] = Risk(_manufacturer, _productCode, _period);

        uint256[] memory payoutOptions = new uint256[](3);
        payoutOptions[0] = _price.div(4);
        payoutOptions[1] = _price.div(2);
        payoutOptions[2] = _price;

        uint256 applicationId = _newApplication(
            _bpExternalKey,
            _premium,
            _currency,
            payoutOptions
        );

        uint256 voteId = voting.newVote(
            abi.encodePacked(uint32(1)),
            string(
                abi.encodePacked(
                    "Underwrite application ",
                    uint256ToString(applicationId)
                )
            ),
            false,
            false
        );
        voteIdToApplicationId[voteId] = applicationId;
        applicationIdToVoteId[applicationId] = voteId;

        emit LogNewVotingForApplication(applicationId, voteId);

        emit LogNewApplication(applicationId);
    }

    function resolveApplication(uint256 _voteId) external {
        (bool open, bool executed, , , , , , , , ) = voting.getVote(_voteId);

        require(open == false, "ERROR::VOTING_IS_NOT_FINISHED");

        if (executed == true) {
            uint256 policyId = _underwrite(voteIdToApplicationId[_voteId]);
            emit LogNewPolicy(policyId);
        } else {
            _decline(voteIdToApplicationId[_voteId]);
            emit LogApplicationDeclined(voteIdToApplicationId[_voteId]);
        }
    }

    function createClaim(uint256 _policyId) external onlySandbox {
        uint256 claimId = _newClaim(_policyId);
        emit LogNewClaim(_policyId, claimId);
    }

    function confirmClaim(uint256 _applicationId, uint256 _claimId, uint256 _payoutOption) external onlySandbox {
        uint256[] memory payoutOptions = _getPayoutOptions(_applicationId);
        uint256 payoutId = _confirmClaim(_claimId, payoutOptions[_payoutOption]);

        emit LogNewPayoutRequired(payoutId);
    }

    function declineClaim(uint256 _claimId) external onlySandbox {

    }

    function confirmPayout(uint256 _payoutId, uint256 _amount) external onlySandbox {
        _payout(_payoutId, _amount);

        emit LogPayout(_payoutId, _amount);
    }

    /* Helper function to convert uint256 to string  */
    function uint256ToString(uint256 value) internal pure returns (string memory) {
        if (value == 0) {
            return "0";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        uint256 index = digits - 1;
        temp = value;
        while (temp != 0) {
            buffer[index--] = byte(uint8(48 + temp % 10));
            temp /= 10;
        }
        return string(buffer);
    }
}
