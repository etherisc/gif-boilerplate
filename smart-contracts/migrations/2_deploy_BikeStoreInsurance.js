const BikeStoreInsurance = artifacts.require("BikeStoreInsurance");
const BikeStoreInsuranceWithVoting = artifacts.require("BikeStoreInsuranceWithVoting");

// 1 part
module.exports = deployer => deployer.deploy(
    BikeStoreInsurance,
    process.env.GIF_PRODUCT_SERVICE_CONTRACT,
);

// 2 part
// module.exports = deployer => deployer.deploy(
//     BikeStoreInsuranceWithVoting,
//     process.env.GIF_PRODUCT_SERVICE_CONTRACT,
//     process.env.ARAGON_VOTING_CONTRACT
// );
